rootProject.name = "Crepes"

pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://jitpack.io")
    }
}

include("Zenith-Core")