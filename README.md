# Crêpes
- A [Weave](https://github.com/Weave-MC) mod for showing [MinecraftCapes](https://minecraftcapes.net/) users' capes in-game with custom cape support.

<br>

# Usage
1. Press `RSHIFT` to open OneConfig menu.
2. Click on `Crêpes` to open the Crêpes menu.

- You can use `file://` to load local files. For example, `file:///C:/Users/username/Desktop/cape.png`.

<br>

## Installation
1. Download the [Crêpes](#download) mod.
2. Place the jar in your Weave mods directory.
    1. Windows: `%userprofile%\.weave\mods`
    2. Unix: `~/.weave/mods`

<br>

## Build
- Clone the repository.
- Run `./gradlew build` in the root directory.
- The built jar file will be in `build/libs`.

<br>

## Credits
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.
- [MinecraftCapes](https://minecraftcapes.net/) - For the awesome service.

<br>

## Download
- [Package Registry](https://gitlab.com/candicey-weave/crepes/-/packages) - Select the latest version and download the jar file that ended with `-relocated.jar`.

<br>

## License
- Crêpes is licensed under the [GNU General Public License Version 3](https://gitlab.com/candicey-weave/crepes/-/blob/master/LICENSE).
