package com.gitlab.candicey.crepes

import com.gitlab.candicey.crepes.config.CrepesConfig
import com.gitlab.candicey.crepes.data.PlayerData
import com.gitlab.candicey.zenithcore.config.Config
import com.gitlab.candicey.zenithcore.config.ConfigManager
import com.gitlab.candicey.zenithcore.font.FontManager
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.awt.Font

internal val LOGGER: Logger = LogManager.getLogger("Crepes")

internal val configCrepes by lazy { CrepesConfig() }

internal val playerDataList = mutableListOf<PlayerData>()