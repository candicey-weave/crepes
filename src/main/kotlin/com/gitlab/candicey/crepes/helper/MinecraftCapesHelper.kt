package com.gitlab.candicey.crepes.helper

import com.gitlab.candicey.crepes.configCrepes
import com.gitlab.candicey.crepes.getPlayerData
import com.gitlab.candicey.zenithcore.extension.delete
import com.gitlab.candicey.zenithcore.extension.getJson
import com.gitlab.candicey.zenithcore.extension.setData
import com.gitlab.candicey.zenithcore.extension.toHttpUrlConnection
import com.gitlab.candicey.zenithcore.util.runAsync
import net.minecraft.client.entity.AbstractClientPlayer
import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.entity.player.EntityPlayer

object MinecraftCapesHelper {
    private fun getUrl(uuid: String): String {
        return "https://api.minecraftcapes.net/profile/${uuid.delete("-")}"
    }

    fun fetchProfile(entityPlayer: EntityPlayer) {
        runAsync {
            val playerData = getPlayerData(entityPlayer)

            var profileResult: ProfileResult? = null

            runCatching {
                val url = getUrl(playerData.uuid!!.toString())
                profileResult = url.toHttpUrlConnection().setData().getJson<ProfileResult>()
                playerData.apply {
                    hasInfo = true
                    hasCapeGlint = profileResult?.capeGlint ?: false
                    upsideDown = profileResult?.upsideDown ?: false
                }
            }.onFailure {
                it.printStackTrace()
            }

            profileResult?.textures?.let { profileResultTextures ->
                if (entityPlayer is EntityPlayerSP && configCrepes.useCustomCape) {
                    playerData.applyCustomCape(configCrepes.customCapeLink)
                } else {
                    profileResultTextures["cape"]?.let {
                        playerData.applyCape(it)
                    }
                }

                profileResultTextures["ears"]?.let {
                    playerData.applyEars(it)
                }
            } ?: run {
                if (entityPlayer is EntityPlayerSP && configCrepes.useCustomCape) {
                    playerData.applyCustomCape(configCrepes.customCapeLink)
                }
            }
        }
    }

    fun hasCape(abstractClientPlayer: AbstractClientPlayer): Boolean {
        return abstractClientPlayer.locationCape != null || getPlayerData(abstractClientPlayer).getCapeLocation() != null
    }

    data class ProfileResult(
        val capeGlint: Boolean? = false,
        val upsideDown: Boolean? = false,
        val textures: Map<String, String>? = null
    )
}