package com.gitlab.candicey.crepes.hook

import com.gitlab.candicey.crepes.configCrepes
import com.gitlab.candicey.crepes.getPlayerData
import com.gitlab.candicey.zenithcore.util.callStaticsBooleanIf
import net.minecraft.client.entity.AbstractClientPlayer
import net.minecraft.client.renderer.entity.layers.LayerCape
import net.weavemc.loader.api.Hook
import org.objectweb.asm.tree.ClassNode

object LayerCapeHook : Hook("net/minecraft/client/renderer/entity/layers/LayerCape") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStaticsBooleanIf<LayerCapeHook>(
            "doRenderLayer",
        )

        cfg.computeFrames()
    }

    @JvmStatic
    fun onDoRenderLayer(
        layerCape: LayerCape,
        entityLivingBaseIn: AbstractClientPlayer,
        limbSwing: Float,
        limbSwingAmount: Float,
        partialTicks: Float,
        ageInTicks: Float,
        netHeadYaw: Float,
        headPitch: Float,
        scale: Float
    ): Boolean = configCrepes.enabled && configCrepes.isCapeVisible && getPlayerData(entityLivingBaseIn).getCapeLocation() != null
}