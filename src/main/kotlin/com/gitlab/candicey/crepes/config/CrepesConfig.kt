package com.gitlab.candicey.crepes.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.Switch
import cc.polyfrost.oneconfig.config.annotations.Text
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType

data class CrepesConfig(
    @Switch(name = "Show Cape")
    var isCapeVisible: Boolean = true,

    @Switch(name = "Show Ears")
    var isEarsVisible: Boolean = true,

    @Switch(name = "Enchant your cape")
    var selfEnchantedCape: Boolean = true,

    @Switch(name = "Use custom cape")
    var useCustomCape: Boolean = false,

    @Text(name = "Custom cape URL")
    var customCapeLink: String = ""
) : Config(Mod("Crepes", ModType.THIRD_PARTY), "crepes.json") {
    init {
        initialize()
    }
}
