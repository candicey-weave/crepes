package com.gitlab.candicey.crepes

import com.gitlab.candicey.crepes.helper.MinecraftCapesHelper
import com.gitlab.candicey.crepes.hook.LayerCapeHook
import com.gitlab.candicey.crepes.layer.CustomLayerCape
import com.gitlab.candicey.crepes.layer.CustomLayerDeadmau5
import com.gitlab.candicey.zenithcore.event.EntityJoinWorldEvent
import com.gitlab.candicey.zenithcore.extension.add
import com.gitlab.candicey.zenithcore.extension.bipedDeadmau5Head
import com.gitlab.candicey.zenithcore.extension.layerRenderers
import com.gitlab.candicey.zenithcore.extension.skinMap
import com.gitlab.candicey.zenithcore.helper.HookManagerHelper
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.Dependencies.concentra
import net.minecraft.client.model.ModelRenderer
import net.minecraft.entity.player.EntityPlayer
import net.weavemc.loader.api.ModInitializer
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.event.StartGameEvent

class YummyCrepes : ModInitializer {
    override fun preInit() {
        info("Crêpes is loading...")

        ZenithLoader.loadDependencies(
            concentra("crepes"),
        )

        EventBus.subscribe(EntityJoinWorldEvent::class.java) {
            val entity = it.entity
            if (entity !is EntityPlayer) {
                return@subscribe
            }

            MinecraftCapesHelper.fetchProfile(entity)
        }

        EventBus.subscribe(StartGameEvent.Post::class.java) {
            mc.renderManager.skinMap.values.forEach { renderPlayer ->
                renderPlayer.mainModel.apply {
                    bipedDeadmau5Head = ModelRenderer(this, 0, 0).apply {
                        setTextureSize(14, 7)
                        addBox(1.5F, -10.5F, -1.0F, 6, 6, 1, 0.0F)
                        addBox(-7.5F, -10.5F, -1.0F, 6, 6, 1, 0.0F)
                        setRotationPoint(0.0F, 0.0F, 0.0F)
                    }
                }

                renderPlayer.layerRenderers.add(CustomLayerCape(renderPlayer), CustomLayerDeadmau5(renderPlayer))
            }
        }

        HookManagerHelper.hooks.add(
            LayerCapeHook,
        )

        info("Crêpes has been loaded!")
    }
}

