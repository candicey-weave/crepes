package com.gitlab.candicey.crepes.layer

import com.gitlab.candicey.crepes.configCrepes
import com.gitlab.candicey.crepes.getPlayerData
import net.minecraft.client.entity.AbstractClientPlayer
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.entity.RenderPlayer
import net.minecraft.client.renderer.entity.layers.LayerRenderer

class CustomLayerDeadmau5(private val playerRenderer: RenderPlayer) : LayerRenderer<AbstractClientPlayer> {
    override fun doRenderLayer(
        entityLivingBaseIn: AbstractClientPlayer?,
        limbSwing: Float,
        limbSwingAmount: Float,
        partialTicks: Float,
        ageInTicks: Float,
        netHeadYaw: Float,
        headPitch: Float,
        scale: Float
    ) {
        if (entityLivingBaseIn == null || !configCrepes.enabled || !configCrepes.isEarsVisible) {
            return
        }

        val playerData = getPlayerData(entityLivingBaseIn)

        if (playerData.getEarsLocation() == null || entityLivingBaseIn.isInvisible || !entityLivingBaseIn.hasSkin()) {
            return
        }

        playerRenderer.bindTexture(playerData.getEarsLocation())

        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F)

        GlStateManager.pushMatrix()

        if (entityLivingBaseIn.isSneaking) {
            GlStateManager.translate(0.0F, 0.25F, 0.0F)
        }
        GlStateManager.scale(1.3333334F, 1.3333334F, 1.3333334F)

        playerRenderer.mainModel.renderDeadmau5Head(0.0625F)

        GlStateManager.popMatrix()
    }

    override fun shouldCombineTextures(): Boolean {
        return false
    }
}