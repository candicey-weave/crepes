package com.gitlab.candicey.crepes.layer

import com.gitlab.candicey.crepes.configCrepes
import com.gitlab.candicey.crepes.getPlayerData
import com.gitlab.candicey.crepes.helper.MinecraftCapesHelper
import net.minecraft.client.entity.AbstractClientPlayer
import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.client.model.ModelBase
import net.minecraft.client.model.ModelRenderer
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.entity.RenderPlayer
import net.minecraft.client.renderer.entity.layers.LayerRenderer
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.MathHelper
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11

class CustomLayerCape(private val playerRenderer: RenderPlayer) : LayerRenderer<AbstractClientPlayer> {
    private val modelCape = CapeModel()

    companion object {
        val ENCHANTED_ITEM_GLINT_RES = ResourceLocation("textures/misc/enchanted_item_glint.png")
    }

    override fun doRenderLayer(
        entityLivingBaseIn: AbstractClientPlayer,
        limbSwing: Float,
        limbSwingAmount: Float,
        partialTicks: Float,
        ageInTicks: Float,
        netHeadYaw: Float,
        headPitch: Float,
        scale: Float
    ) {
        if (!configCrepes.enabled || !configCrepes.isCapeVisible) {
            return
        }

        val playerData = getPlayerData(entityLivingBaseIn)

        val capeLocation = playerData.getCapeLocation()
        if (playerData.showCape && !entityLivingBaseIn.isInvisible && MinecraftCapesHelper.hasCape(entityLivingBaseIn)) {
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F)
            GlStateManager.enableBlend()
            GlStateManager.blendFunc(GL11.GL_LINES, GL11.GL_POINTS)
            if (capeLocation == null) {
                playerRenderer.bindTexture(entityLivingBaseIn.locationCape)
            } else {
                playerRenderer.bindTexture(capeLocation)
            }

            GlStateManager.pushMatrix()
            GlStateManager.translate(0.0F, 0.0F, 0.125F)
            val d0 = entityLivingBaseIn.prevChasingPosX + (entityLivingBaseIn.chasingPosX - entityLivingBaseIn.prevChasingPosX) * partialTicks.toDouble() - (entityLivingBaseIn.prevPosX + (entityLivingBaseIn.posX - entityLivingBaseIn.prevPosX) * partialTicks.toDouble())
            val d1 = entityLivingBaseIn.prevChasingPosY + (entityLivingBaseIn.chasingPosY - entityLivingBaseIn.prevChasingPosY) * partialTicks.toDouble() - (entityLivingBaseIn.prevPosY + (entityLivingBaseIn.posY - entityLivingBaseIn.prevPosY) * partialTicks.toDouble())
            val d2 = entityLivingBaseIn.prevChasingPosZ + (entityLivingBaseIn.chasingPosZ - entityLivingBaseIn.prevChasingPosZ) * partialTicks.toDouble() - (entityLivingBaseIn.prevPosZ + (entityLivingBaseIn.posZ - entityLivingBaseIn.prevPosZ) * partialTicks.toDouble())
            val f = entityLivingBaseIn.prevRenderYawOffset + (entityLivingBaseIn.renderYawOffset - entityLivingBaseIn.prevRenderYawOffset) * partialTicks
            val d3 = MathHelper.sin(f * Math.PI.toFloat() / 180.0F).toDouble()
            val d4 = (-MathHelper.cos(f * Math.PI.toFloat() / 180.0F)).toDouble()
            var f1 = d1.toFloat() * 10.0F
            f1 = MathHelper.clamp_float(f1, -6.0F, 32.0F)
            val f2 = ((d0 * d3 + d2 * d4).toFloat() * 100.0F).coerceAtLeast(0.0F)
            val f3 = (d0 * d4 - d2 * d3).toFloat() * 100.0F

            val f4 = entityLivingBaseIn.prevCameraYaw + (entityLivingBaseIn.cameraYaw - entityLivingBaseIn.prevCameraYaw) * partialTicks
            f1 += MathHelper.sin((entityLivingBaseIn.prevDistanceWalkedModified + (entityLivingBaseIn.distanceWalkedModified - entityLivingBaseIn.prevDistanceWalkedModified) * partialTicks) * 6.0F) * 32.0F * f4
            if (entityLivingBaseIn.isSneaking) {
                f1 += 25.0F
            }

            GlStateManager.rotate(6.0F + f2 / 2.0F + f1, 1.0F, 0.0F, 0.0F)
            GlStateManager.rotate(f3 / 2.0F, 0.0F, 0.0F, 1.0F)
            GlStateManager.rotate(-f3 / 2.0F, 0.0F, 1.0F, 0.0F)
            GlStateManager.rotate(180.0F, 0.0F, 1.0F, 0.0F)

            modelCape.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale, entityLivingBaseIn)
            modelCape.render(entityLivingBaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale)
            if ((playerData.hasCapeGlint || (entityLivingBaseIn is EntityPlayerSP && configCrepes.selfEnchantedCape)) && capeLocation != null) {
                renderEnchantmentGlint(entityLivingBaseIn, modelCape, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, scale)
            }

            GlStateManager.disableBlend()
            GlStateManager.popMatrix()
        }
    }

    private fun renderEnchantmentGlint(
        entityLivingBaseIn: AbstractClientPlayer,
        modelBase: ModelBase,
        limbSwing: Float,
        limbSwingAmount: Float,
        partialTicks: Float,
        ageInTicks: Float,
        netHeadYaw: Float,
        headPitch: Float,
        scale: Float
    ) {
        val f = entityLivingBaseIn.ticksExisted + partialTicks
        playerRenderer.bindTexture(ENCHANTED_ITEM_GLINT_RES)
        GlStateManager.enableBlend()
        GlStateManager.depthFunc(GL11.GL_EQUAL)
        GlStateManager.depthMask(false)
        val f1 = 0.5F
        GlStateManager.color(f1, f1, f1, 1.0F)

        for (i in 0..1) {
            GlStateManager.disableLighting()
            GlStateManager.blendFunc(GL11.GL_SRC_COLOR, GL11.GL_ONE)
            val f2 = 0.76F
            GlStateManager.color(0.5F * f2, 0.25F * f2, 0.8F * f2, 1.0F)
            GlStateManager.matrixMode(GL11.GL_TEXTURE)
            GlStateManager.loadIdentity()
            val f3 = f * (0.001F + i * 0.003F) * 20.0F
            val f4 = 0.33333334F
            GlStateManager.scale(f4, f4, f4)
            GlStateManager.rotate(30.0F - i * 60.0F, 0.0F, 0.0F, 1.0F)
            GlStateManager.translate(0.0F, f3, 0.0F)
            GlStateManager.matrixMode(GL11.GL_MODELVIEW)
            modelBase.render(entityLivingBaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale)
        }

        GlStateManager.matrixMode(GL11.GL_TEXTURE)
        GlStateManager.loadIdentity()
        GlStateManager.matrixMode(GL11.GL_MODELVIEW)
        GlStateManager.enableLighting()
        GlStateManager.depthMask(true)
        GlStateManager.depthFunc(GL11.GL_LEQUAL)
        GlStateManager.disableBlend()
    }

    override fun shouldCombineTextures(): Boolean {
        return false
    }

    class CapeModel : ModelBase() {
        private val capeRenderer = ModelRenderer(this, 0, 0)

        init {
            capeRenderer.run {
                setTextureSize(64, 32)
                addBox(-5.0F, 0.0F, -1.0F, 10, 16, 1)
            }
        }

        override fun render(
            entityIn: Entity,
            limbSwing: Float,
            limbSwingAmount: Float,
            ageInTicks: Float,
            netHeadYaw: Float,
            headPitch: Float,
            scale: Float
        ) {
            super.render(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale)

            capeRenderer.render(scale)
        }

        override fun setRotationAngles(
            limbSwing: Float,
            limbSwingAmount: Float,
            ageInTicks: Float,
            netHeadYaw: Float,
            headPitch: Float,
            scaleFactor: Float,
            entityIn: Entity
        ) {
            super.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor, entityIn)

            val entityPlayer = entityIn as EntityPlayer
            if (entityPlayer.getCurrentArmor(2) == null) {
                if (entityPlayer.isSneaking) {
                    capeRenderer.apply {
                        rotationPointZ = 1.4F
                        rotationPointY = 1.85F
                    }
                } else {
                    capeRenderer.apply {
                        rotationPointZ = 0.0F
                        rotationPointY = 0.0F
                    }
                }
            } else if (entityPlayer.isSneaking) {
                capeRenderer.apply {
                    rotationPointZ = 0.3F
                    rotationPointY = 0.8F
                }
            } else {
                capeRenderer.apply {
                    rotationPointZ = -1.1F
                    rotationPointY = -0.85F
                }
            }
        }
    }
}