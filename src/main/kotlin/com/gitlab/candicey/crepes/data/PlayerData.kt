package com.gitlab.candicey.crepes.data

import com.gitlab.candicey.crepes.info
import com.gitlab.candicey.zenithcore.mc
import net.minecraft.client.renderer.texture.DynamicTexture
import net.minecraft.util.ResourceLocation
import java.awt.image.BufferedImage
import java.net.URL
import java.util.*
import javax.imageio.ImageIO
import javax.imageio.ImageReader
import kotlin.properties.Delegates

data class PlayerData(
    var hasCape: Boolean = false,
    var hasAnimatedCape: Boolean = false,
    var hasEars: Boolean = false,
    var showCape: Boolean = true,
    var hasCapeGlint: Boolean = false,
    var upsideDown: Boolean = false,
    var hasInfo: Boolean = false,
    var uuid: UUID? = null,
    var animatedCape: Map<Int, BufferedImage>? = null,
    var lastFrameTime: Long = 0L,
    var lastFrame: Int = 0,
    var capeInterval: Int = 100
) {
    private fun readTexture(texture: String): BufferedImage? {
        runCatching {
            Base64.getDecoder().decode(texture).inputStream().use {
                return ImageIO.read(it)
            }
        }.onFailure {
            it.printStackTrace()
        }

        return null
    }

    private fun readLinkToTexture(link: String): List<BufferedImage> {
        runCatching {
            val imageInputStream = ImageIO.createImageInputStream(URL(link).openStream())

            var gif = false
            var imageReader by Delegates.notNull<ImageReader>()

            ImageIO.getImageReaders(imageInputStream).forEach {
                if (it.formatName == "gif") {
                    gif = true
                    imageReader = it
                }
            }

            if (gif) {
                imageReader.input = imageInputStream

                val frames = mutableListOf<BufferedImage>()

                for (i in 0 until imageReader.getNumImages(true)) {
                    frames.add(imageReader.read(i))
                }

                return frames
            } else {
                return listOf(ImageIO.read(imageInputStream))
            }
        }

        return listOf()
    }

    fun applyCape(capeData: String) {
        processCape(readTexture(capeData) ?: return)
    }

    fun applyCustomCape(capeLink: String) {
        val capes = readLinkToTexture(capeLink)

        if (capes.size == 1) {
            processCape(capes[0])
        } else if (capes.size > 1) {
            processGifCape(capes)
        }
    }

    private fun processCape(capeImage: BufferedImage) {
        if (capeImage.height == capeImage.width / 2) {
            // static cape
            var imageWidth = 64
            var imageHeight = 32

            while (imageWidth < capeImage.width || imageHeight < capeImage.height) {
                imageWidth *= 2
                imageHeight *= 2
            }

            val newCape = BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB)
            val graphics = newCape.graphics
            graphics.drawImage(capeImage, 0, 0, null)
            graphics.dispose()
            applyTexture(ResourceLocation("Crepes", "capes/${uuid.toString()}"), newCape)

            hasCape = true
            hasAnimatedCape = false

            info("Applied static cape for ${uuid.toString()}")
        } else {
            // animated cape
            val capes = mutableMapOf<Int, BufferedImage>()
            val imageHeight = capeImage.height / (capeImage.width / 2)

            for (i in 0 until imageHeight) {
                val frame = BufferedImage(capeImage.width, capeImage.width / 2, BufferedImage.TYPE_INT_ARGB)
                val frameGraphics = frame.graphics
                frameGraphics.drawImage(capeImage, 0, 0, capeImage.width, capeImage.width / 2, 0, i * (capeImage.width / 2), capeImage.width, (i + 1) * (capeImage.width / 2), null)
                frameGraphics.dispose()
                capes[i] = frame
            }

            animatedCape = capes
            hasCape = true
            hasAnimatedCape = true

            capes.forEach {
                applyTexture(ResourceLocation("Crepes", "capes/${uuid.toString()}/${it.key}"), it.value)
            }

            info("Applied animated cape for ${uuid.toString()} (${capes.size} frames)")
        }
    }

    private fun processGifCape(capeImage: List<BufferedImage>) {
        animatedCape = capeImage.mapIndexed { index, bufferedImage ->
            index to bufferedImage
        }.toMap()
        hasCape = true
        hasAnimatedCape = true

        animatedCape!!.forEach {
            applyTexture(ResourceLocation("Crepes", "capes/${uuid.toString()}/${it.key}"), it.value)
        }

        info("Applied custom animated cape for ${uuid.toString()} (${capeImage.size} frames)")
    }

    fun applyEars(earsData: String) {
        val earImage = readTexture(earsData) ?: return

        applyTexture(ResourceLocation("Crepes", "ears/${uuid.toString()}"), earImage)

        hasEars = true
    }

    private fun applyTexture(resourceLocation: ResourceLocation, bufferedImage: BufferedImage) {
        mc.addScheduledTask {
            mc.textureManager.loadTexture(resourceLocation, DynamicTexture(bufferedImage))
        }
    }

    private fun getFrame(): ResourceLocation {
        val time = System.currentTimeMillis()
        return if (time > lastFrameTime + capeInterval) {
            val currentFrameNumber = if (lastFrame + 1 >= animatedCape!!.size) 0 else lastFrame + 1
            lastFrame = currentFrameNumber
            lastFrameTime = time
            ResourceLocation("Crepes", "capes/${uuid.toString()}/$currentFrameNumber")
        } else {
            ResourceLocation("Crepes", "capes/${uuid.toString()}/$lastFrame")
        }
    }

    fun getCapeLocation(): ResourceLocation? {
        return if (hasCape) {
            if (hasAnimatedCape) {
                getFrame()
            } else {
                ResourceLocation("Crepes", "capes/${uuid.toString()}")
            }
        } else {
            null
        }
    }

    fun getEarsLocation(): ResourceLocation? {
        return if (hasEars) {
            ResourceLocation("Crepes", "ears/${uuid.toString()}")
        } else {
            null
        }
    }
}
