package com.gitlab.candicey.crepes

import com.gitlab.candicey.crepes.data.PlayerData
import net.minecraft.entity.player.EntityPlayer

internal fun info(message: String) = LOGGER.info("[Crepes] $message")

internal fun getPlayerData(entityPlayer: EntityPlayer) = playerDataList.firstOrNull { it.uuid == entityPlayer.uniqueID } ?: PlayerData(uuid = entityPlayer.uniqueID).also { playerDataList.add(it) }